# Image Recognition
Il s'agit d'une application web qui fournit une API pour la reconnaissance d'images en utilisant l'API Clarifai.<br>
Télécharger une image ou saisir l'URL d'une image pour afficher sous forme de liste les différents concepts contenu dans cette image.

## Table des matières
- [Image Recognition](#image-recognition)
  - [Table des matières](#table-des-matières)
  - [Installation](#installation)
      - [Clonez le projet depuis le dépôt GitLab](#clonez-le-projet-depuis-le-dépôt-gitlab)
      - [Creation du fichier .env](#creation-du-fichier-env)
      - [Installation des dépendances (back-end)](#installation-des-dépendances-back-end)
      - [Installation des dépendances (front-end)](#installation-des-dépendances-front-end)
  - [Utilisation](#utilisation)
  - [Auteur](#auteur)
  - [Licence](#licence)


## Installation
Suivez les étapes ci-dessous pour installer et configurer le projet :

#### Clonez le projet depuis le dépôt GitLab

```shell
$ git clone https://gitlab.com/deb-brunier/image-recognition.git

$ cd image-recognition
```

#### Creation du fichier .env
Créez un fichier .env à la racine du projet et ajoutez-y la variable d'environnement suivante
```shell
CLARIFAI_KEY=ici_votre_clé_api_clarifai
```

#### Installation des dépendances (back-end)
A la racine du projet, exécutez la commande suivante :
```shell
$ npm i

$ npm start
```

#### Installation des dépendances (front-end)
```shell
$ cd frontend

$ npm i

$ npm start
```

## Utilisation
Consulter l'application depuis le navigateur internet: [http://localhost:3000](http://localhost:3000).

## Auteur
Ce projet a été développé par [_Déb_](https://gitlab.com/deb-brunier).


## Licence
Ce projet est sous licence [MIT](LICENSE). Vous pouvez consulter le fichier [LICENSE](LICENSE) pour plus de détails.