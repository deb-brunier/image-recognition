import React, { useState } from "react";
import axios from "axios";

import {
  FormControl,
  Button,
  ButtonGroup,
  Form,
  Stack,
  ToggleButton,
} from "react-bootstrap";

const inputOptions = [
  {
    name: "Saisir l'URL de l'image",
    value: "imageURL",
  },
  {
    name: "Téléverser une image",
    value: "uploadImage",
  },
];

export default function InputImage(props) {
  const { setOutputs, setImageToPredict } = props;
  const [inputOption, setInputOption] = useState("imageURL");
  const [imageUrl, setImageUrl] = useState(
    "https://s3.amazonaws.com/samples.clarifai.com/featured-models/general-elephants.jpg"
  );
  const [fileObj, setFileObj] = useState(null);

  const handleChangeImageUrl = (e) => {
    setImageUrl(e.target.value);
  };

  const predictImage = () => {
    setOutputs([]);
    setImageToPredict(imageUrl);
    axios
      .post("http://localhost:3001/predict", {
        imageUrl: imageUrl,
      })
      .then((res) => {
        setOutputs(res.data.results);
      })
      .catch((err) => {
        alert(err);
      });
  };

  const predictImageViaUpload = () => {
    setOutputs([]);
    const formData = new FormData();
    formData.append("file", fileObj);
    const reader = new FileReader();
    reader.addEventListener("load", function () {
      setImageToPredict(reader.result);
    });

    if (fileObj) {
      reader.readAsDataURL(fileObj);
    }

    axios
      .post("http://localhost:3001/predict/upload", formData)
      .then((res) => {
        setOutputs(res.data.results);
      })
      .catch((err) => {
        alert(err);
      });
  };

  const handleFileFormControlOnChange = (e) => {
    if (e.target.files.length) {
      setFileObj(e.target.files[0]);
    }
  };

  return (
    <Stack gap={3}>
      <ButtonGroup>
        {inputOptions.map((io) => {
          return (
            <ToggleButton
              key={io.value}
              checked={inputOption === io.value}
              value={io.value}
              type="radio"
              variant={
                inputOption === io.value
                  ? "outline-dark"
                  : "outline-secondary"
              }
              onClick={(e) => {
                setInputOption(io.value);
              }}
            >
              {io.name}
            </ToggleButton>
          );
        })}
      </ButtonGroup>

      {inputOption === "imageURL" ? (
        <div>
          <FormControl
            className="mb-3"
            value={imageUrl}
            placeholder="URL de l'image"
            aria-label="URL de l'image"
            onChange={handleChangeImageUrl}
          />
          <Button variant="dark" onClick={predictImage}>
            Analyser
          </Button>
        </div>
      ) : (
        <div>
          <Form.Group controlId="file" className="mb-3">
            <Form.Control
              type="file"
              onChange={handleFileFormControlOnChange}
            />
          </Form.Group>
          <Button variant="dark" onClick={predictImageViaUpload}>Valider</Button>
        </div>
      )}
    </Stack>
  );
}
