import { useState } from "react";
import { Container, Stack, Navbar } from "react-bootstrap"
import InputImage from "./InputImage";
import Output from "./Output";

function App() {
  const [outputs, setOutputs] = useState([]);
  const [imageToPredict, setImageToPredict] = useState("");

  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand className="mx-auto" >
          Image Recognition
        </Navbar.Brand>
      </Navbar>
        <div className="mt-5" />

        <Container>
        <Stack gap={2}>
          <p>Format de l'image : jpg, jpeg ou png.
          Poids maximum : 20 mo.</p>
          <InputImage setOutputs={setOutputs} setImageToPredict={setImageToPredict} />
          <Output outputs={outputs} imageToPredict={imageToPredict} />
        </Stack>
      </Container>
    </div>

  );
}

export default App;